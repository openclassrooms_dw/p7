const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user')
const password = require('../middlewares/password')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup', password, userCtrl.signup)
router.post('/login', userCtrl.login)

module.exports = router;